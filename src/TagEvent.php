<?php

namespace Drupal\autotagger;

use Symfony\Component\EventDispatcher\Event;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Class TagEvent.
 *
 * @package Drupal\autotagger
 */
class TagEvent extends Event {
  const TAG = 'event.tag';

  protected $node;

  protected $field;

  protected $term;

  protected $isDenied = FALSE;

  /**
   * TagEvent constructor.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Full node object that should be tagged.
   * @param string $field
   *   Machine name of the field to hold the term.
   * @param \Drupal\taxonomy\TermInterface $term
   *   Term to be added to $field on $node.
   */
  public function __construct(NodeInterface $node, $field, TermInterface $term) {
    $this->node = $node;
    $this->field = $field;
    $this->term = $term;
  }

  /**
   * Returns the node that is to be tagged.
   *
   * @return \Drupal\node\NodeInterface
   *   The node object that is the target of the tagging.
   */
  public function getNode() {
    return $this->node;
  }

  /**
   * Returns the field to which the tag will be added.
   *
   * @return string
   *   The machine name of the destination field.
   */
  public function getField() {
    return $this->field;
  }

  /**
   * Returns the term object.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   The term object for the term to be added.
   */
  public function getTerm() {
    return $this->term;
  }

  /**
   * Allows the subscriber to change out the term.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term object for the term to be added.
   */
  public function setTerm(TermInterface $term) {
    $this->term = $term;
  }

  /**
   * Allows the subscriber to abort the tagging.
   *
   * @return bool
   *   True if the tagging should be aborted.
   */
  public function isDenied() {
    return $this->isDenied;
  }

  /**
   * Allows the subscriber to abort the tagging.
   */
  public function deny() {
    $this->isDenied = TRUE;
  }

}
